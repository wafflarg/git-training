#!/usr/bin/env pyhton3
import argparse
from .main import main

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--name", help="person to greet")
    args = parser.parse_args()

    ret = main(name=args.name)
    exit(ret)
